# NSFW Commands | Cog built by Discord Coding Community

# Discord
import discord

# Red
from redbot.core import commands

# Libs
import aiohttp

BaseCog = getattr(commands, "Cog", object)


class NSFW(BaseCog):   
  

    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()

    @commands.group(autohelp=True, aliases=["ph", "phub"])
    @commands.guild_only()
    @commands.is_nsfw()
    async def pornhub(self, ctx):
        """
        Pornhub Commands!
        If `[p]help pornhub` or any other pornhub commands are used in a non-nsfw channel,
        you will not be able to see the list of commands for this category.
        """
        pass
         
    @pornhub.command(aliases=["rndm", "rvid"])
    async def random(self, ctx):
        """Get a random video from pornhub"""

        url = "http://pornhub.com/random"
        async with self.session.get(url) as r:
            await ctx.send(r.url) 

    def cog_unload(self):
        self.bot.loop.create_task(self.session.close())

    __del__ = cog_unload
