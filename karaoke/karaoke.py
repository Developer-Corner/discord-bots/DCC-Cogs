# Karaoke Commands | Cog built Discord Coding Community

# Discord
import discord

# Red
from redbot.core import commands

# Libs
import aiohttp

BaseCog = getattr(commands, "Cog", object)


class Karaoke(BaseCog):   
  

    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()

    @commands.group(autohelp=True])
    @commands.guild_only()
    async def karaoke(self, ctx):
        """
        Karaoke Commands!
        A full list of commands for the Karaoke system.
        """
        pass
         
    @karaoke.command()
    async def start(self, ctx):
        """Start a karaoke event"""
        await

    @karaoke.command()
    async def enter(self, ctx):
        """Join an existing karaoke event"""

    @karaoke.command()
    async def sing(self, ctx):
        """Select a song to sing during a karaoke event"""

    @karaoke.command()
    async def end(self, ctx):
        """Stop the currently playing track."""

    @karaoke.command()
    async def vote(self, ctx):
        """Select a song to sing during a karaoke event"""

    def cog_unload(self):
        self.bot.loop.create_task(self.session.close())

    __del__ = cog_unload
